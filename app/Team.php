<?php

namespace Lobiro;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'owner_id',
    ];

    public function owner()
    {
        return $this->belongsTo(User::class)->withDefault();
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_team');
    }
}
