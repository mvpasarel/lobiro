<?php

namespace Lobiro\Http\Controllers;

use Hash;
use Illuminate\Auth\Events\Registered;
use Lobiro\Http\Requests\UserAssignTeamRequest;
use Lobiro\Http\Requests\UserCreateRequest;
use Lobiro\Http\Requests\UserUpdateRequest;
use Lobiro\Http\Resources\UserCollection;
use Lobiro\Http\Resources\UserResource;
use Lobiro\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Lobiro\Http\Resources\UserCollection
     */
    public function index()
    {
        return new UserCollection(User::paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Lobiro\Http\Requests\UserCreateRequest $request
     * @return \Lobiro\Http\Resources\UserResource
     */
    public function store(UserCreateRequest $request)
    {
        $data = $request->all();

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        event(new Registered($user));

        $user->generateToken();

        return new UserResource($user);

    }

    /**
     * Display the specified resource.
     *
     * @param  \Lobiro\User $user
     * @return \Lobiro\Http\Resources\UserResource
     */
    public function show(User $user)
    {
        return new UserResource($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Lobiro\Http\Requests\UserUpdateRequest $request
     * @param  \Lobiro\User $user
     * @return \Lobiro\Http\Resources\UserResource
     */
    public function update(UserUpdateRequest $request, User $user)
    {
        $user->update($request->all());

        return new UserResource($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Lobiro\User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(User $user)
    {
        $user->delete();

        return response()->json(null, 204);
    }

    /**
     * Assign team to current user
     *
     * @param  \Lobiro\Http\Requests\UserAssignTeamRequest $request
     * @param  \Lobiro\User $user
     * @return \Lobiro\Http\Resources\UserResource
     */
    public function assignTeam(UserAssignTeamRequest $request, User $user)
    {
        $user->teams()->attach($request->get('team_id'));

        return new UserResource($user);
    }
}
