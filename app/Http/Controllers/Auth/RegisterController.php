<?php

namespace Lobiro\Http\Controllers\Auth;

use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Lobiro\Http\Controllers\Controller;
use Lobiro\Http\Requests\UserCreateRequest;
use Lobiro\Http\Resources\UserResource;
use Lobiro\User;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \Lobiro\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function register(UserCreateRequest $request)
    {
        event(new Registered($user = $this->create($request->all())));

        return $this->registered($request, $user);
    }

    /**
     * @inheritdoc
     */
    protected function registered($request, $user)
    {
        $user->generateToken();

        return new UserResource($user);
    }
}
