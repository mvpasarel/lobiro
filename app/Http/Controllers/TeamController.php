<?php

namespace Lobiro\Http\Controllers;

use Lobiro\Http\Requests\TeamAssignUserRequest;
use Lobiro\Http\Requests\TeamCreateRequest;
use Lobiro\Http\Requests\TeamUpdateRequest;
use Lobiro\Http\Resources\TeamCollection;
use Lobiro\Http\Resources\TeamResource;
use Lobiro\Team;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Lobiro\Http\Resources\TeamCollection
     */
    public function index()
    {
        return new TeamCollection(Team::paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Lobiro\Http\Requests\TeamCreateRequest $request
     * @return \Lobiro\Http\Resources\TeamResource
     */
    public function store(TeamCreateRequest $request)
    {
        $data = $request->all();

        $team = Team::create([
            'title' => $data['title'],
            'owner_id' => $request->user()->id,
        ]);

        return new TeamResource($team);

    }

    /**
     * Display the specified resource.
     *
     * @param  \Lobiro\Team $team
     * @return \Lobiro\Http\Resources\TeamResource
     */
    public function show(Team $team)
    {
        return new TeamResource($team);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Lobiro\Http\Requests\TeamUpdateRequest $request
     * @param  \Lobiro\Team $team
     * @return \Lobiro\Http\Resources\TeamResource
     */
    public function update(TeamUpdateRequest $request, Team $team)
    {
        $team->update($request->all());

        return new TeamResource($team);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Lobiro\Team $team
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Team $team)
    {
        $team->delete();

        return response()->json(null, 204);
    }

    /**
     * Assign user to current team
     *
     * @param  \Lobiro\Http\Requests\TeamAssignUserRequest $request
     * @param  \Lobiro\Team $team
     * @return \Lobiro\Http\Resources\TeamResource
     */
    public function assignUser(TeamAssignUserRequest $request, Team $team)
    {
        $team->users()->attach($request->get('user_id'));

        return new TeamResource($team);
    }
}
