<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::fallback(function () {
    return response()->json(['errors' => ['message' => 'Not Found.']], 404);
})->name('api.fallback.404');

Route::middleware('auth:api')->get('/me', function (Request $request) {
    return new \Lobiro\Http\Resources\UserResource($request->user());
});

Route::post('register', 'Auth\RegisterController@register');

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('users', 'UserController@index');
    Route::get('users/{user}', 'UserController@show');
    Route::post('users', 'UserController@store');
    Route::put('users/{user}', 'UserController@update');
    Route::delete('users/{user}', 'UserController@destroy');

    Route::get('teams', 'TeamController@index');
    Route::get('teams/{team}', 'TeamController@show');
    Route::post('teams', 'TeamController@store');
    Route::put('teams/{team}', 'TeamController@update');
    Route::delete('teams/{team}', 'TeamController@destroy');

    Route::put('users/{user}/assign-team', 'UserController@assignTeam');
    Route::put('teams/{team}/assign-user', 'TeamController@assignUser');
});
