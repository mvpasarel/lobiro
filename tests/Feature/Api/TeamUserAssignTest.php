<?php

namespace Tests\Feature\Api;

use Lobiro\Team;
use Lobiro\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\JsonResponse;

class TeamUserAssignTest extends TestCase
{

    public function testsApiUserCanAttachUserToTeam()
    {
        $apiUser = factory(User::class)->create();

        $team = factory(Team::class)->create(['owner_id' => $apiUser->id]);
        $user = factory(User::class)->create();

        $data = [
            'user_id' => $user->id,
        ];
        $response = $this->putJson('/api/teams/' . $team->id . '/assign-user?api_token=' . $apiUser->api_token, $data);

        $this->assertEquals(JsonResponse::HTTP_OK, $response->status());

        $response = json_decode($response->content(), true);
        $this->assertArrayHasKey('data', $response);
        $this->assertEquals(['id', 'title', 'owner'], array_keys($response['data']));
    }

    public function testsApiUserCanAttachTeamToUser()
    {
        $apiUser = factory(User::class)->create();

        $team = factory(Team::class)->create(['owner_id' => $apiUser->id]);
        $user = factory(User::class)->create();

        $data = [
            'team_id' => $team->id,
        ];
        $response = $this->putJson('/api/users/' . $user->id . '/assign-team?api_token=' . $apiUser->api_token, $data);

        $this->assertEquals(JsonResponse::HTTP_OK, $response->status());

        $response = json_decode($response->content(), true);

        $this->assertArrayHasKey('data', $response);
        $this->assertEquals(['id', 'name', 'email', 'api_token', 'teams'], array_keys($response['data']));
    }
}
