<?php

namespace Tests\Feature\Api;

use Lobiro\Team;
use Lobiro\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\JsonResponse;

class TeamCRUDTest extends TestCase
{

    public function testUserCanCreateTeamUsingApiToken()
    {
        $apiUser = factory(User::class)->create();

        $data = [
            'title' => 'First Team',
        ];

        $response = $this->postJson('/api/teams?api_token=' . $apiUser->api_token, $data);

        $this->assertEquals(JsonResponse::HTTP_CREATED, $response->status());

        $response = json_decode($response->content(), true);

        $this->assertArrayHasKey('data', $response);
        $this->assertEquals(['id', 'title', 'owner'], array_keys($response['data']));

        $this->assertEquals($response['data']['title'], 'First Team');
        $this->assertEquals($response['data']['owner']['id'], $apiUser->id);
    }

    public function testsApiUserCanUpdateTeam()
    {
        $apiUser = factory(User::class)->create();

        $team = factory(Team::class)->create();
        $data = [
            'title' => 'Iorem Lipsum',
        ];

        $response = $this->putJson('/api/teams/' . $team->id . '?api_token=' . $apiUser->api_token, $data);

        $this->assertEquals(JsonResponse::HTTP_OK, $response->status());

        $response = json_decode($response->content(), true);

        $this->assertArrayHasKey('data', $response);
        $this->assertEquals(['id', 'title', 'owner'], array_keys($response['data']));

        $this->assertEquals($response['data']['title'], 'Iorem Lipsum');
    }

    public function testsApiUserCanDeleteTeam()
    {
        $apiUser = factory(User::class)->create();

        $team = factory(Team::class)->create();

        $response = $this->deleteJson('/api/teams/' . $team->id . '?api_token=' . $apiUser->api_token);

        $this->assertEquals(JsonResponse::HTTP_NO_CONTENT, $response->status());
    }

    public function testsApiUserCanSeeAllTeams()
    {
        $apiUser = factory(User::class)->create();

        factory(Team::class)->create();
        factory(Team::class)->create();
        factory(Team::class)->create();

        $response = $this->getJson('/api/teams?api_token=' . $apiUser->api_token);

        $this->assertEquals(JsonResponse::HTTP_OK, $response->status());

        $response = json_decode($response->content(), true);

        $this->assertArrayHasKey('data', $response);
        $this->assertArrayHasKey('links', $response);
        $this->assertArrayHasKey('meta', $response);

        $this->assertCount(3, $response['data']);
        $this->assertEquals(['id', 'title', 'owner'], array_keys($response['data'][0]));
        $this->assertEquals(['id', 'title', 'owner'], array_keys($response['data'][1]));
        $this->assertEquals(['id', 'title', 'owner'], array_keys($response['data'][2]));
    }
}
