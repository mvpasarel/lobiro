<?php

namespace Tests\Feature\Api;

use Lobiro\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UnauthenticatedTest extends TestCase
{
    public function testUserCantAccessProtectedEndpointsWithoutToken()
    {
        $this->json('GET', '/api/users')->assertStatus(401);
    }

    public function testUserCantAccessProtectedEndpointsWithoutCorrectToken()
    {
        $this->json('GET', '/api/users?api_token=WRONG')->assertStatus(401);
    }
}
