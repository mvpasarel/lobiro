<?php

namespace Tests\Feature\Api;

use Lobiro\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\JsonResponse;

class UserCRUDTest extends TestCase
{

    public function testUserCanCreateAnotherUserUsingApiToken()
    {
        $apiUser = factory(User::class)->create();

        $data = [
            'name' => 'First Last',
            'email' => 'validdd@email.com',
            'password' => 'MyAw3s0mePa$$',
            'password_confirmation' => 'MyAw3s0mePa$$',
        ];

        $response = $this->postJson('/api/users?api_token=' . $apiUser->api_token, $data);

        $this->assertEquals(JsonResponse::HTTP_CREATED, $response->status());

        $response = json_decode($response->content(), true);

        $this->assertArrayHasKey('data', $response);
        $this->assertEquals(['id', 'name', 'email', 'api_token', 'teams'], array_keys($response['data']));

        $this->assertEquals($response['data']['name'], 'First Last');
        $this->assertEquals($response['data']['email'], 'validdd@email.com');
    }

    public function testsApiUserCanUpdateUser()
    {
        $apiUser = factory(User::class)->create();

        $user = factory(User::class)->create();
        $data = [
            'name' => 'Iorem Lipsum',
        ];

        $response = $this->putJson('/api/users/' . $user->id . '?api_token=' . $apiUser->api_token, $data);

        $this->assertEquals(JsonResponse::HTTP_OK, $response->status());

        $response = json_decode($response->content(), true);

        $this->assertArrayHasKey('data', $response);
        $this->assertEquals(['id', 'name', 'email', 'api_token', 'teams'], array_keys($response['data']));

        $this->assertEquals($response['data']['name'], 'Iorem Lipsum');
    }

    public function testsApiUserCanDeleteUser()
    {
        $apiUser = factory(User::class)->create();

        $user = factory(User::class)->create();

        $response = $this->deleteJson('/api/users/' . $user->id . '?api_token=' . $apiUser->api_token);

        $this->assertEquals(JsonResponse::HTTP_NO_CONTENT, $response->status());
    }

    public function testsApiUserCanSeeAllUsers()
    {
        $apiUser = factory(User::class)->create();

        factory(User::class)->create();
        factory(User::class)->create();
        factory(User::class)->create();

        $response = $this->getJson('/api/users?api_token=' . $apiUser->api_token);

        $this->assertEquals(JsonResponse::HTTP_OK, $response->status());

        $response = json_decode($response->content(), true);

        $this->assertArrayHasKey('data', $response);
        $this->assertArrayHasKey('links', $response);
        $this->assertArrayHasKey('meta', $response);

        $this->assertCount(4, $response['data']);
        $this->assertEquals(['id', 'name', 'email', 'api_token', 'teams'], array_keys($response['data'][0]));
        $this->assertEquals(['id', 'name', 'email', 'api_token', 'teams'], array_keys($response['data'][1]));
        $this->assertEquals(['id', 'name', 'email', 'api_token', 'teams'], array_keys($response['data'][2]));
        $this->assertEquals(['id', 'name', 'email', 'api_token', 'teams'], array_keys($response['data'][3]));
    }
}
