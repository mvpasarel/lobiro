<?php

namespace Tests\Feature\Api;

use Lobiro\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class NotFoundRouteTest extends TestCase
{

    public function testNotFoundRouteReturnsJsonWith404()
    {
        $this->withoutExceptionHandling();
        $response = $this->get('/api/not-found');

        $response->assertStatus(404);
        $response->assertHeader('Content-Type', 'application/json');
        $response->assertJson([
            'errors' => [
                'message' => 'Not Found.',
            ],
        ]);
    }

    /**
     * @expectedException \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function testUserNotFoundTriggersModelNotFoundException()
    {
        $this->withoutExceptionHandling();

        $user = factory(User::class)->create();
        $this->assertNotNull($user->api_token);
        $this->json('GET', '/api/users/10101010', ['api_token' => $user->api_token]);
    }

    /**
     * @expectedException \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function testTeamNotFoundTriggersModelNotFoundException()
    {
        $this->withoutExceptionHandling();

        $user = factory(User::class)->create();
        $this->assertNotNull($user->api_token);
        $this->json('GET', '/api/teams/10101010', ['api_token' => $user->api_token]);
    }
}
